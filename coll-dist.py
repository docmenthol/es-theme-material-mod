'''
Collects from, and distributes backgrounds to, the individual system
directories (nes, snes, fba, etc.). Really eases the job of ensuring
that no backgrounds are used more than once.
'''

import os
import sys
import glob
import shutil

skip = ['.git', 'art', 'collected']

def collect_task(directories):
    if not os.path.isdir('collected'):
        os.mkdir('collected')
    missing = []
    for d in directories:
        bgs = glob.glob('{}/bg.*'.format(d))
        if len(bgs) == 0:
            missing.append(d)
            continue
        fext = os.path.splitext(os.path.basename(bgs[0]))
        f = 'collected/{}{}'.format(fext[0].replace('bg', d), fext[1])
        print('{} -> {}'.format(bgs[0], f))
        shutil.move(bgs[0], f)

    if len(missing) > 0:
        with open('collected/missing.txt', 'w') as f:
            f.write('\n'.join(missing))

def distribute_task(directories):
    if not os.path.isdir('collected'):
        os.mkdir('collected')
    for d in directories:
        bgs = glob.glob('collected/{}.*'.format(d))
        if len(bgs) == 0:
            continue
        fext = os.path.splitext(os.path.basename(bgs[0]))
        f = '{}/bg{}'.format(d, fext[1])
        print('{} -> {}'.format(bgs[0], f))
        shutil.move(bgs[0], f)

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Usage: python coll-dist.py <(c)ollect|(d)istribute>')
        sys.exit(1)

    task = sys.argv[1].lower()[0]

    if not task in ['c', 'd']:
        print('Invalid operation: {}'.format(sys.argv[1]))
        print('Valid operations: (c)ollect, (d)istribute')
        sys.exit(1)

    directories = list(os.walk('.'))[0][1]
    for s in skip:
        try:
            directories.remove(s)
        except:
            pass  # super ignore powers

    if task == 'c':
        collect_task(directories)
    elif task == 'd':
        distribute_task(directories)