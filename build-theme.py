'''
Converts existing theme.xml files into the new format.
'''

import os
import sys
import glob
from bs4 import BeautifulSoup
from PIL import Image

skip = ['.git', 'art', 'collected']

def theme_build_task(directories, dummy=True):
    with open('template.xml') as t:
        template = t.read()

    for d in directories:
        print('-> {}'.format(d), end=': ')

        logo_t = glob.glob('{0}/{0}.svg'.format(d))
        logo = os.path.basename(logo_t[0])

        bg_t = glob.glob('{}/bg.*'.format(d))
        if len(bg_t) == 0:
            print('! missing background !')
            continue
        else:
            bg = bg_t[0]

        ext = os.path.splitext(bg)[1][1:]

        with open('{}/theme.xml'.format(d)) as theme:
            theme_xml = theme.read()

        soup = BeautifulSoup(theme_xml, 'xml')

        pos = soup.select('image[name=logo] > pos')[0].text
        max_size = soup.select('image[name=logo] > maxSize')[0].text
        selector_color = soup.select('textlist[name=gamelist] > selectorColor')[0].text

        img_pos = '0.1475 0.3705'
        img_size = ''
        img_max_size = '0.245 0.301'

        box_art = soup.select('image[name=md_image]')
        if len(box_art) > 0:
            img_pos = soup.select('image[name=md_image] > pos')[0].text
            img_max_size = soup.select('image[name=md_image] > maxSize')[0].text
            img_size_t = soup.select('image[name=md_image] > size')
            if len(img_size_t) > 0:
                img_size = img_size_t[0].text

        template_vars = {
            'name': d, 'ext': ext, 'pos': pos, 'max_size': max_size,
            'img_pos': img_pos, 'img_size': img_size,
            'img_max_size': img_max_size, 'selector_color': selector_color
        }

        if dummy:
            of = '{}/theme.gen.xml'.format(d)
        else:
            of = '{}/theme.xml'.format(d)

            img = Image.open(bg)

            hfile = '{}/header.{}'.format(d, ext)
            hsize = (0, 0, 1920, 200)
            header = img.crop(hsize)
            header.save(hfile)
            print('header,', end=' ')

            ffile = '{}/footer.{}'.format(d, ext)
            fsize = (0, img.size[1] - 125, 1920, 1080)
            footer = img.crop(fsize)
            footer.save(ffile)
            print('footer,', end=' ')

            img.close()

        with open(of, 'w') as f:
            f.write(template.format(**template_vars))
        print('theme')

if __name__ == '__main__':
    dummy = True
    if len(sys.argv) > 1 and sys.argv[1] == 'doit':
        dummy = False

    directories = list(os.walk('.'))[0][1]
    for s in skip:
        try:
            directories.remove(s)
        except:
            pass  # super ignore powers

    theme_build_task(directories, dummy)